#denoising script
#author: funnel

#t ia2 -adj tidx [ev_idx][nf_idx][shutter_idx]", "-1 disable"
#looks like the values are between 0-16383 (0x0000-0x3FFF)
#If you want to set noise reduction value to 1024
t ia2 -adj tidx -1 1024 -1

# off noise reduction:
# t ia2 -adj tidx -1 0 -1

t cal -jqlt 100



# # From Fried
# #vibrance/saturation adjustments
# t ia2 -adj ev 10 0 130 0 0 190 0

# #shadow/highlight clipping adjustments
# t ia2 -adj l_expo 163

# t ia2 -adj ae 160

# t ia2 -adj autoknee 250

# #set gamma level
# t ia2 -adj gamma 80
# #enable 14 scene mode
# t cal -sc 14
# #enable raw+jpeg stills
# #t app test debug_dump 14
# #fix ae/awb/adj locks
# t ia2 -3a 1 1 0 1




#--Sharpness Adjustments--
# Coring
t is2 -shp mode 0
t is2 -shp max_change 5 5
t is2 -shp cor d:\script_inc\coring.txt
sleep 1
