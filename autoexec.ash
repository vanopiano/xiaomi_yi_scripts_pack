#============================================================#
# 					Xiaomi Yi SCRIPT                     #
#			Video Bitrate & Other utility v0.3         #
#					   by AirKite and De_Vano              #
#============================================================#
# 					!!!WARNING!!!                        #
# 			  Firmware requirements: 1.2.13          #
#============================================================#

#============================================================#
#             MODULES                            #
#============================================================#
sleep 1;  d:\script_inc\modules\info_start.ash
          d:\script_inc\modules\vbitrate.ash
          d:\script_inc\modules\flimit.ash
          d:\script_inc\modules\photo_iso100.ash
          d:\script_inc\modules\photo_raw.ash
          d:\script_inc\modules\awb_off.ash
          d:\script_inc\modules\reducenoise_off.ash
          d:\script_inc\modules\remove_junk.ash.ash

sleep 5; 	d:\script_inc\modules\info_finish.ash
sleep 1;	d:\script_inc\modules\autorecord.ash
#============================================================#
#END SCRIPT
